import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() destino:DestinoViaje;
  @HostBinding('attr.class') cssClass = 'col-md-4 mt-3';

  constructor() { }

  ngOnInit(): void {
    console.log(this.destino);
  }

}

import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {

  destino:DestinoViaje = new DestinoViaje('', '');

  destinos:Array<DestinoViaje> = [
    { nombre: 'Viaje 1', url: 'https://placeimg.com/380/230/nature'},
    { nombre: 'Viaje 2', url: 'https://placeimg.com/380/230/nature'},
    { nombre: 'Viaje 3', url: 'https://placeimg.com/380/230/nature'},
    { nombre: 'Viaje 4', url: 'https://placeimg.com/380/230/nature'}
  ];

  constructor() { }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
    
    const _item:DestinoViaje = new DestinoViaje(nombre,url);
    this.destinos.push(_item);

    this.destino.nombre = '';
    this.destino.url = '';

    return false;
  }

}
